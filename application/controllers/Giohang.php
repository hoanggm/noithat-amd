<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giohang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Sanpham_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		$this->load->view('site_views/giohang_view');
	}

	public function addto_cart($code)
	{
		$item['product'] = $this->Sanpham_Model->getbycode($code);

		$price= $this->Cal_price($item['product'][0]['price'],$item['product'][0]['discount']);
		$name = $item['product'][0]['name'];
		$img = $item['product'][0]['img_link'];
		$data = array(
			'id'      => $code,
			'qty'     => 1,
			'price'   => $price,
			'name'    => $name,
			'img'     =>$img
		);
		
		$chk = $this->cart->insert($data);

		redirect('Giohang','refresh');
	}

	public function update_cart()
	{
		$id=$this->input->post('num-id');
		$number=$this->input->post('num-order');

		if($number > 0) {

			$data = array(
				'rowid' => $id,
				'qty'   => $number
			);

			$update=$this->cart->update($data);
		}

		else {

			$data = array(
				'rowid' => $id,
				'qty'   => 0
			);

			$update=$this->cart->update($data);
		}

		redirect('Giohang','refresh');
	}

	public function removeall()
	{
		$this->cart->destroy();

		redirect('Giohang','refresh');
	}

	public function Cal_price($price , $discount)
	{
		return round($price - ( ($discount * $price)/100 ));
	}


}

/* End of file Giohang.php */
/* Location: ./application/controllers/Giohang.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thongke extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Thongke_Model');
	}

	public function getThongke()
	{
		$list= $this->Thongke_Model->getThongke();	

		//create response data
		$response=[];
		array_push($response, ['list' => $list]);

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
		
	}

}

/* End of file Thongke.php */
/* Location: ./application/controllers/Thongke.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trangchu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
        $this->load->model('Sanpham_Model');
        $this->load->model('Danhmuc_Model');
        $this->load->model('Hang_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		$data['itemshot'] = $this->Sanpham_Model->get_itemshot();
		$data['itemsnew'] = $this->Sanpham_Model->get_itemsnew();
		$data['itemssale'] = $this->Sanpham_Model->get_itemssale();

		$this->load->view('site_views/trangchu_view',$data);
	}
}

/* End of file Trangchu.php */
/* Location: ./application/controllers/Trangchu.php */

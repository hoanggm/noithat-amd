<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Taikhoan_Model');
		$this->load->model('Thongke_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{

		if (!empty($_SESSION['username'])) {
			$data['countOrders']=count($this->Thongke_Model->getCountNewOrders());
			$data['countUsers']=count($this->Thongke_Model->getCountNewUsers());
			$this->load->view('admin_views/trangchu_view',$data);
		}

		else {
			
			$this->load->view('admin_views/dangnhap_view');
		}
		

	}

	public function login()
	{

		if (empty($_SESSION['username'])) {

			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$ru = $this->input->post('ran1');

			for ($i = 0; $i <= $ru ; $i++) {
				
				$username = base64_decode($username);
			}

			$rp = $this->input->post('ran2');

			for ($i = 0; $i <= $rp ; $i++) {
				
				$password = base64_decode($password);
			}

			$password = sha1($password);

			$res = $this->Taikhoan_Model->getlogin($username,$password);

			if (count($res) != 0) {

				$this->session->set_userdata('username',$res[0]['username']);
				$this->session->set_userdata('password',$res[0]['password']);

				$this->session->sess_expiration = '1440000000';

				$item = ['islogin' => 1];
				$this->Taikhoan_Model->update( $item, $res[0]['id']);
			}
			else {

				echo '<script type="text/javascript" charset="utf-8" >
				alert("Tài khoản không tồn tại !!!");
				</script>';
			}
		}


		redirect('Admin','refresh');
	}

	public function logout()
	{

		$res = $this->Taikhoan_Model->getlogin($_SESSION['username'] , $_SESSION['password']);

		$item = ['islogin' => 0];
		$this->Taikhoan_Model->update( $item, $res[0]['id']);

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');

		redirect('Admin','refresh');
	}

	public function rand_string( $length ) {
		$str='';
		$chars = "qwertyuiopASDFGHJKLzxcvbnm0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */

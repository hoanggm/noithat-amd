<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trangtintuc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tintuc_Model');
	}

	public function index()
	{
		$total_rows = count($this->Tintuc_Model->get());
		$per_page = 15;


		$this->load->library('pagination');

		$config['base_url'] = base_url().'Trangtintuc/index';;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';


		$config['next_link'] = '»';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '«';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';


		$config['cur_tag_open'] = '<li>';
		$config['cur_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$page = $this->pagination->create_links();

		$uri_seg = $this->uri->segment(3);

		$data['all'] = $this->Tintuc_Model->getLimit($per_page,$uri_seg);
		$data['page'] = $page;

		$this->load->view('site_views/trangtin_view', $data);
	}

	public function getDetail($id)
	{
		$data['detail']= $this->Tintuc_Model->getbyid($id);
		$this->load->view('site_views/cttin_view', $data);
	}

}

/* End of file Trangtintuc.php */
/* Location: ./application/controllers/Trangtintuc.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thanhtoan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Giaodich_Model');
		$this->load->model('Chitietgiaodich_Model');
		$this->load->model('Giamgia_Model');
		$this->load->model('Khachhang_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if (!empty($_SESSION['customer'])) {
			
			$data['cus'] = $this->Khachhang_Model->get($_SESSION['id']);
			$this->load->view('site_views/thanhtoan_view',$data);
		}

		else {

			$this->load->view('site_views/thanhtoan_view');
		}
	}

	public function send_code()
	{
		$email = $this->input->post('email');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$discount = $this->input->post('discount');

		$scode = $this->rand_string(5);
		$today = date('Y-m-d');

		$total = $this->cart->total();

		if($discount != '') {
			$data = $this->Giamgia_Model->get(array('id' => $discount));

			if($data['date_end'] < $today) {

				$discount = '';
			}
		}

		$content ='';
		$content .='Khách hàng : '.$name.'<br>';
		$content .='Địa chỉ : '.$address.'<br><br>';
		$content .='Thông tin đơn hàng<br>';
		$content .='<table >';
		$content .='<thead>';
		$content .='<td style="color: red;">Mã sản phẩm</td>';
		$content .='<td style="color: red;">Tên sản phẩm</td>';
		$content .='<td style="color: red;">Số lượng</td>';
		$content .='<td style="color: red;">Đơn giá</td>';
		$content .='<td style="color: red;">Thành tiền</td>';
		$content .='</thead><br><tbody>';

		foreach (($this->cart->contents()) as $item) {

			$content .='<tr>';
			$content .='<td style="width: 200px;"><p>'.$item['id'].'</p></td>';
			$content .='<td  style="width: 200px;"><p>'.$item['name'].'</p></td>';
			$content .='<td  style="width: 100px;">'.$item['qty'].'</td>';
			$content .='<td style="width: 200px;">
			<p>'.($this->cart->format_number(round($item['price']))).' đ</p></td>';
			$content .='<td><p>'.($this->cart->format_number($item['subtotal'])).' đ</p></td>';
			$content .='</tr>';
			$content .='<br>';
		}

		$content .='</tbody></table><br>';
		$content .='<p style="color: red;"> Tổng tiền trước giảm giá : ';
		$content .= ($this->cart->format_number($total)).' đ</p>';
		$content .='</table>';
		

		if ($discount != '') {
			
			$content .='<p>Mã giảm giá : '.$discount.'</p>';
			$content .='<p>Giảm giá: '.$data['percent'].' %</p>';
			$content .='<p style="color: red;">Tổng tiền sau giảm giá : ';
			$content .= ($this->cart->format_number($this->Cal_price($total,$data['percent']))).' đ</p>';
		}
		else {

			$content .='<p>Mã giảm giá : Không có mã / Mã hết hạn</p>';
			$content .='<p>Giảm giá: 0 %</p>';
			$content .='<p style="color: red;">Tổng tiền sau giảm giá : ';
			$content .= ($this->cart->format_number($total)).' đ</p>';
		}
		
		$content .= '<br><p style="color:red;">Mã xác nhận giao dịch của bạn là : '.$scode.'</p>';
		$content .= '<br><p style="color: red;">Cảm ơn đã đặt hàng tại Webbanhang.abc !!!!</p>';

		try {

			$this->send_mail($content,$email);

			$_SESSION['scode'] = $scode;

			echo "Vui lòng kiểm tra email của bạn";
			
		} catch (Exception $e) {
			
			echo 'Thất bại !!! Vui lòng thử lại';

		}
	}

	private function Cal_price( $price , $discount)
	{
		$res = round($price - ( ($discount * $price)/100 ));

		return $res;
	}


	public function send_mail($mess,$to)
	{
		$this->load->library('email');
		//0

		$mail_config['smtp_host'] = 'smtp.gmail.com';
		$mail_config['smtp_port'] = '587';
		$mail_config['smtp_user'] = 'myserverhoanggiap@gmail.com';
		$mail_config['_smtp_auth'] = TRUE;
		$mail_config['smtp_pass'] = 'yxwubgkgdxzstpkd';
		$mail_config['smtp_crypto'] = 'tls';
		$mail_config['protocol'] = 'smtp';
		$mail_config['mailtype'] = 'html';
		$mail_config['send_multipart'] = FALSE;
		$mail_config['charset'] = 'utf-8';
		$mail_config['wordwrap'] = TRUE;

		$this->email->initialize($mail_config);

		$this->email->set_newline("\r\n");

		$this->email->from($mail_config['smtp_user'], 'Webbanhang.abc');
		$this->email->to($to);

        // $this->email->cc('another@example.com');
        // $this->email->bcc('and@another.com');

		$this->email->subject('Xác nhận đặt hàng');
		$this->email->message($mess);

		$this->email->send();
	}

	public function rand_string( $length ) {
		$str='';
		$chars = "0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

	public function submit()
	{
		$data = $this->input->post();

		if($data['maxacthuc'] == $_SESSION['scode']) {
			$total = $this->cart->total();
		
			$today = date('Y-m-d');

			if($data['magiamgia'] != '') {

				$chk = $this->Giamgia_Model->get(array('id' => $data['magiamgia']));

				if($chk['date_end'] < $today) {

					$discount = 0;
				}
				else {

					$discount = $chk['percent'];
				}
			}

			$amount = $this->Cal_price($total,$discount);

			$tran = [

				'user_name'  => $data['hoten'],
				'user_email' => $data['email'],
				'user_phone' => $data['sodienthoai'],
				'user_address' => $data['diachi'],
				'amount'     => $amount,
				'payment'    => $data['hinhthuc'],
				'message'    => $data['yeucaukhac'],
				'scode'      => $_SESSION['scode'],
				'discount_code'=> $data['magiamgia'],
				'statuss'    => 0,
			];

			$this->Giaodich_Model->insert($tran);

			$transaction_id = $this->db->insert_id();

			foreach (($this->cart->contents()) as $item) {

				$tran_info = [

					'transactions_id' => $transaction_id,
					'products_code'     => $item['id'],
					'price'     => $item['price'],
					'qty'       => $item['qty'],
					'amount'    => $item['subtotal'],
				];


				$this->Chitietgiaodich_Model->insert($tran_info);
			}

			$this->cart->destroy();
			$this->session->unset_userdata('scode');

			echo '<script type="text/javascript" charset="utf-8">alert("Đặt hàng thành công !!!!");</script>';
		}

		else {
			
			echo '<script type="text/javascript" charset="utf-8">alert("Mã xác thực sai !!!!");</script>';
		}

		redirect('Thanhtoan','refresh');

	}
}

/* End of file Thanhtoan.php */
/* Location: ./application/controllers/Thanhtoan.php */

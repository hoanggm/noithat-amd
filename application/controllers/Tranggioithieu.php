<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tranggioithieu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('site_views/gioithieu_view');
	}

}

/* End of file Tranggioithieu.php */
/* Location: ./application/controllers/Tranggioithieu.php */
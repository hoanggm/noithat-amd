<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taikhoan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Taikhoan_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if (!empty($_SESSION['username'])) {
			
			$total_rows = count($this->Taikhoan_Model->get());
			$per_page = 10;


			$this->load->library('pagination');

			$config['base_url'] = base_url().'Hang/index';;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = 3;
			$config['num_links'] = 3;

			$config['num_tag_open'] = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';


			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';


			$config['cur_tag_open'] = '<li class="page-item page-link" style="border-color:#17a2b8;">';
			$config['cur_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = $this->pagination->create_links();

			$uri_seg = $this->uri->segment(3);

			$data['all']=$this->Taikhoan_Model->getLimit($per_page,$uri_seg);
			$data['page'] = $page;


			$this->load->view('admin_views/taikhoan_view',$data);
		}

		else {

			redirect('Admin','refresh');
		}
		
	}

	public function multidel()
	{
		if (!empty($_SESSION['username'])) {
			
			$ids = $this->input->post('checked_id');

			if($ids != NULL) {

				$res = $this->Taikhoan_Model->multidelete($ids);

				if ($res) {

					$this->session->set_flashdata('user_su','Thao tác thành công !!!');
					$this->session->set_flashdata('user_er','');
					$this->session->set_flashdata('user_wr','');
				}

				else {


					$this->session->set_flashdata('user_su','');
					$this->session->set_flashdata('user_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('user_wr','');
				}
			}

			else {
				
				$this->session->set_flashdata('user_su','');
				$this->session->set_flashdata('user_er','');
				$this->session->set_flashdata('user_wr','Chưa chọn các bản ghi cần xóa !!!');

			}

			redirect('Taikhoan','refresh');
		}

		else {

			$this->index();
		}
		
	}
	

	// Add a new item
	public function add()
	{
		if (!empty($_SESSION['username'])) {
			
			$data = $this->input->post();

			$pass = sha1($data['password']);

			$item = 
			[

				'name' => $data['name'],
				'username' => $data['username'],
				'password' => $pass,
				'email' => $data['email'],
				'phone' => $data['phone'],
			];

			if ($this->Taikhoan_Model->chkcontain_username($data['username']) == false) {

				$res = $this->Taikhoan_Model->insert($item);

				if ($res) {


					$this->session->set_flashdata('user_su','Thao tác thành công !!!');
					$this->session->set_flashdata('user_er','');
					$this->session->set_flashdata('user_wr','');

				}
				else {

					$this->session->set_flashdata('user_su','');
					$this->session->set_flashdata('user_wr','');
					$this->session->set_flashdata('user_er','Thao tác thất bại !!!');
				}

			}

			else {

				$this->session->set_flashdata('user_su','');
				$this->session->set_flashdata('user_wr','Tên đăng nhập đã tồn tại !!! Không thể thêm mới');
				$this->session->set_flashdata('user_er','');

			}


			redirect('Taikhoan','refresh');
		}

		else {

			$this->index();
		}
		
	}

	//Update one item
	public function update( $id = NULL )
	{
		if(!empty($_SESSION['username'])){

			$data = $this->input->post();
			$id = $this->input->post('id');

			$update_user = ['id' => $id];
			$get_user = $this->Taikhoan_Model->get($update_user);

			if ($data['username'] == $data['oldusername']) {

				if ($data['newpass'] != '') {

					$npass = sha1($data['newpass']);

					$item = 
					[

						'name' => $data['name'],
						'username' => $data['username'],
						'password' => $npass,
						'email' => $data['email'],
						'phone' => $data['phone'],
					];
				}

				else {

					$item = 
					[

						'name' => $data['name'],
						'username' => $data['username'],
						'password' => $get_user['password'],
						'email' => $data['email'],
						'phone' => $data['phone'],
					];
				}


				$res = $this->Taikhoan_Model->update($item,$data['id']);

				$this->session->set_flashdata('user_su','Thao tác thành công !!!');
				$this->session->set_flashdata('user_er','');
				$this->session->set_flashdata('user_wr','');


			}

			else {

				$chk = $this->Taikhoan_Model->chkcontain_username($data['username']);

				if ($chk == true) {

					$this->session->set_flashdata('user_su','');
					$this->session->set_flashdata('user_wr','Tên đăng nhập đã tồn tại !!! Không thể cập nhật');
					$this->session->set_flashdata('user_er','');

				}

				else {

					if ($data['newpass'] != '') {

						$npass = sha1($data['newpass']);

						$item = 
						[

							'name' => $data['name'],
							'username' => $data['username'],
							'password' => $npass,
							'email' => $data['email'],
							'phone' => $data['phone'],
						];
					}

					else {

						$item = 
						[

							'name' => $data['name'],
							'username' => $data['username'],
							'password' => $get_user['password'],
							'email' => $data['email'],
							'phone' => $data['phone'],
						];
					}


					$res = $this->Taikhoan_Model->update($item,$data['id']);

					if ($res) {
						
						$this->session->set_flashdata('user_su','Thao tác thành công !!!');
						$this->session->set_flashdata('user_er','');
						$this->session->set_flashdata('user_wr','');
					}

					else {

						$this->session->set_flashdata('user_su','');
						$this->session->set_flashdata('user_er','Thao tác thất bại !!!');
						$this->session->set_flashdata('user_wr','');

					}
					

				}
			}

			redirect('Taikhoan','refresh');
		}

		else {

			$this->index();
		}
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		if(!empty($_SESSION['username'])) {

			$id=$this->input->post('id');

			$res = $this->Taikhoan_Model->delete($id);

			if ($res) {

				$this->session->set_flashdata('user_su','Thao tác thành công !!!');
				$this->session->set_flashdata('user_er','');

			}

			else {


				$this->session->set_flashdata('user_su','');
				$this->session->set_flashdata('user_er','Thao tác thất bại !!!');

			}

			redirect('Taikhoan','refresh');
		}

		else {

			$this->index();

		}

	}

}

/* End of file Taikhoan.php */
/* Location: ./application/controllers/Taikhoan.php */

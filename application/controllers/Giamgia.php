<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giamgia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Giamgia_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if(!empty($_SESSION['username'])) {

			$data['all'] = $this->Giamgia_Model->get();
			$this->load->view('admin_views/magiamgia_view', $data);
		}

		else {
			redirect('Admin','refresh');
		}
	}

	public function rand_string( $length ) {
		$str='';
		$chars = "ABCDEFGHIKLMNOPQRTW0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

	// Add a new item
	public function add()
	{
		if(!empty($_SESSION['username'])) {

			$id = $this->rand_string(10);
			$today = date('Y-m-d');


			$data = $this->input->post();

			
			$item = [
				'id' => $id , 
				'percent' => $data['Phantram'] , 
				'date_start' => $today ,
				'date_end' => $data['Hethan'] 
			];

			$res = $this->Giamgia_Model->insert($item);

			if ($res) {

				$this->session->set_flashdata('sale_su','Thao tác thành công !!!');
				$this->session->set_flashdata('sale_er','');
				$this->session->set_flashdata('sale_wr','');

			}

			else {

				$this->session->set_flashdata('sale_su','');
				$this->session->set_flashdata('sale_er','Thao tác thất bại !!!');
				$this->session->set_flashdata('sale_wr','');
			}

			redirect('Giamgia','refresh');
		}

		else {

			$this->index();
		}
	}

	//Delete one item
	public function multidel()
	{
		if (!empty($_SESSION['username'])) {
			
			$ids = $this->input->post('checked_id');

			if ($ids != NULL) {
				
				$res = $this->Giamgia_Model->multidelete($ids);

				if ($res) {

					$this->session->set_flashdata('sale_su','Thao tác thành công !!!');
					$this->session->set_flashdata('sale_er','');
					$this->session->set_flashdata('sale_wr','');

				}

				else {


					$this->session->set_flashdata('sale_su','');
					$this->session->set_flashdata('sale_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('sale_wr','');

				}
			}

			else {
				
				$this->session->set_flashdata('sale_su','');
				$this->session->set_flashdata('sale_er','');
				$this->session->set_flashdata('sale_wr','Chưa chọn các bản ghi cần xóa !!!');
			}

			redirect('Giamgia','refresh');
		}

		else {
			
			$this->index();
		}
		
	}
}

/* End of file Giamgia.php */
/* Location: ./application/controllers/Giamgia.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giaodich extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Giaodich_Model');
		$this->load->model('Chitietgiaodich_Model');
		$this->load->model('Sanpham_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if (!empty($_SESSION['username'])) {

			$total_rows = count($this->Giaodich_Model->get());
			$per_page = 10;


			$this->load->library('pagination');

			$config['base_url'] = base_url().'Sanpham/index';;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = 3;
			$config['num_links'] = 3;

			$config['num_tag_open'] = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';


			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';


			$config['cur_tag_open'] = '<li class="page-item page-link" style="border-color:#17a2b8;">';
			$config['cur_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = $this->pagination->create_links();

			$uri_seg = $this->uri->segment(3);

			$data['all'] = $this->Giaodich_Model->getLimit($per_page,$uri_seg);
			$data['page'] = $page;

			$this->load->view('admin_views/giaodich_view',$data);
		}

		else {

			redirect('Admin','refresh');
		}
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		if (!empty($_SESSION['username'])) {
			
			$id = $this->input->post('id');

			$res = $this->Giaodich_Model->delete($id);

			$this->Chitietgiaodich_Model->delete(array('transactions_id' => $id));

			if ($res) {

				$this->session->set_flashdata('gg_su','Thao tác thành công !!!');
				$this->session->set_flashdata('gg_er','');

			}

			else {


				$this->session->set_flashdata('gg_su','');
				$this->session->set_flashdata('gg_er','Thao tác thất bại !!!');

			}

			redirect('Giaodich','refresh');
		}

		else {
			
			$this->index();
		}
	}

	public function multidel()
	{

		if (!empty($_SESSION['username'])) {
			
			$ids = $this->input->post('checked_id');

			if ($ids != NULL) {
				
				$res = $this->Giaodich_Model->multidelete($ids);

				foreach ($ids as $value) {
					
					$this->Chitietgiaodich_Model->delete(array('transactions_id' => $value));
				}

				if ($res) {

					$this->session->set_flashdata('gg_su','Thao tác thành công !!!');
					$this->session->set_flashdata('gg_er','');
					$this->session->set_flashdata('gg_wr','');

				}

				else {


					$this->session->set_flashdata('gg_su','');
					$this->session->set_flashdata('gg_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('gg_wr','');

				}
			}

			else {
				
				$this->session->set_flashdata('gg_su','');
				$this->session->set_flashdata('gg_er','');
				$this->session->set_flashdata('gg_wr','Chưa chọn các bản ghi cần xóa !!!');
			}

			redirect('Giaodich','refresh');
		}

		else {
			
			$this->index();
		}
		
	}

	public function detailitem($id) {

		if (!empty($_SESSION['username'])) {

			$data['detail'] = $this->Chitietgiaodich_Model->get(array('transactions_id' => $id));

			$tran = $this->Giaodich_Model->get(array('transactions.id' => $id));

			$data['pay'] = $tran[0]['payment'];
			$data['status'] = $tran[0]['statuss'];

			$sum = $this->Chitietgiaodich_Model->get_sum($id);
			$data['sum'] = $sum[0]['amount'];

			$data['tran'] = $tran;
			$data['tran_id'] = $id;

			$this->load->view('admin_views/ctgiaodich_view', $data);
		}

		else {
			
			$this->index();
		}
	}

	public function update_statuss() {

		if (!empty($_SESSION['username'])) {

			$data = $this->input->post();

			$item = ['statuss' => $data['capnhattrangthai']];

			$res = $this->Giaodich_Model->update($item,$data['id']);

			if ($res) {

				$this->session->set_flashdata('gg_su','Thao tác thành công !!!');
				$this->session->set_flashdata('gg_er','');
				$this->session->set_flashdata('gg_wr','');

			}

			else {


				$this->session->set_flashdata('gg_su','');
				$this->session->set_flashdata('gg_er','Thao tác thất bại !!!');
				$this->session->set_flashdata('gg_wr','');

			}

			redirect('Giaodich','refresh');
		}

		else {
			
			$this->index();
		}
		
	}

}

/* End of file Giaodich.php */
/* Location: ./application/controllers/Giaodich.php */

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sanpham_Model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'product';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
    	$this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id , product.id as product_id');
      $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
      $this->db->from(self::TABLE_NAME);
      if ($where !== NULL) {
        if (is_array($where)) {
         foreach ($where as $field=>$value) {
          $this->db->where($field, $value);
        }
      } else {
       $this->db->where(self::PRI_INDEX, $where);
     }
   }
   $result = $this->db->get()->result_array();
   if ($result) {
    if ($where !== NULL) {
     return array_shift($result);
   } else {
     return $result;
   }
 } else {
  return $result;
}
}

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function multidelete($id) {
    	if(is_array($id)){
    		$this->db->where_in('id', $id);
    	}else{
    		$this->db->where('id', $id);
    	}
    	$delete = $this->db->delete(self::TABLE_NAME);
    	return $delete?true:false;
    }

    public function getLimit($limit, $offset)
    {
      $this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id ,
       product.id as product_id');
      $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
      $this->db->order_by('created', 'desc');
      $data = $this->db->get(self::TABLE_NAME, $limit, $offset);
      $data = $data->result_array();

      return $data;
    }

    public function getbyid($id)
    {
      $this->db->where('product.id', $id);
      $this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id ,
       product.id as product_id');
      $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
      $data = $this->db->get(self::TABLE_NAME);
      $data = $data->result_array();

      return $data;
    }

    public function getbycode($code)
    {
     $this->db->where('code', $code);
     $this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id ,
       product.id as product_id , design_catalog.name as com_name , design_catalog.id as com_id');
     $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
     $this->db->join('design_catalog', 'product.com_id = design_catalog.id', 'left');
     $data = $this->db->get(self::TABLE_NAME);
     $data = $data->result_array();

     return $data;
   }

   public function get_itemshot()
   {
     $this->db->select('*');
     $this->db->order_by('count_buy', 'desc');
     return $this->db->get(self::TABLE_NAME,9)->result_array();
   }

   public function get_itemsnew()
   {
     $this->db->where('discount = 0');
     $this->db->select('*');
     $this->db->order_by('created', 'desc');
     return $this->db->get(self::TABLE_NAME,6)->result_array();
   }

   public function get_itemssale()
   {
     $this->db->where('discount > 0');
     $this->db->select('*');
     $this->db->order_by('rand()', 'desc');
     return $this->db->get(self::TABLE_NAME,6)->result_array();
   }

   public function get_item_samecat($id_cat,$id)
   {
    $this->db->where('catalog_id', $id_cat);
    $this->db->where(self::TABLE_NAME.'.id != ', $id);
    $this->db->select('*');
    $this->db->order_by('created', 'desc');
    return $this->db->get(self::TABLE_NAME,10)->result_array();
  }

  public function getbycatalog($id_cat)
  {
    $this->db->where('catalog_id', $id_cat);
    $this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id ,
     product.id as product_id');
    $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
    $this->db->order_by('created', 'desc');
    $data = $this->db->get(self::TABLE_NAME);

    $data = $data->result_array();

    $content = '<thead>';
    $content .= '<th style="width: 10px">Chọn</th>';
    $content .= '<th>Mã sản phẩm</th>';
    $content .= '<th>Tên sản phẩm</th>';
    $content .= '<th>Danh mục</th>';
    $content .= '<th>Giá</th>';
    $content .= ' <th>Giảm giá</th>';
    $content .= '<th style="width: auto;">Tác vụ</th>';
    $content .= '<tbody id="mytable">';

    foreach ($data as $value) {

      $price = round($value['price']);

      $content .='<tr>';
      $content .=' <td align="center">';
      $content .='<label class="container-check">';
      $content .=' <input type="checkbox"  name="checked_id[]" value="'.$value['product_id'].'">';
      $content .='<span class="checkmark"></span>';
      $content .='</label></td>';
      $content .='<td>'.$value['code'].'</td>';
      $content .='<td>'.$value['name'].'</td>';
      $content .='<td>'.$value['cat_name'].'</td>';
      $content .=' <td>'.$price.'</td>';
      $content .='<td>'.$value['discount'].' %</td>';
      $content .='<td><a title="Chi tiết" ';
      $content .='class="btn btn-sm btn-outline-info" ';
      $content  .='href="'.base_url().'Sanpham/detailitem/'.$value['product_id'].'">';
      $content .='<i class="fas fa-pen"></i></a> ';
      $content .='| <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"';
      $content .=' data-toggle="modal" data-target="#del'.$value['product_id'].'">';
      $content .=' <i class="fas fa-trash-alt"></i>';
      $content .=' </button></td>';
      $content .='</tr>';
    }

    $content .='</tbody>';

    return $content;
  }

  public function getbycom($id_com)
  {
    $this->db->where('com_id', $id_com);
    $this->db->select(self::TABLE_NAME.'.*, catalog.name as cat_name , catalog.id as cat_id ,
     product.id as product_id');
    $this->db->join('catalog', 'product.catalog_id = catalog.id', 'left');
    $this->db->order_by('created', 'desc');
    $data = $this->db->get(self::TABLE_NAME);

    $data = $data->result_array();

    $content = '<thead>';
    $content .= '<th style="width: 10px">Chọn</th>';
    $content .= '<th>Mã sản phẩm</th>';
    $content .= '<th>Tên sản phẩm</th>';
    $content .= '<th>Danh mục</th>';
    $content .= '<th>Giá</th>';
    $content .= ' <th>Giảm giá</th>';
    $content .= '<th style="width: auto;">Tác vụ</th>';
    $content .= '<tbody id="mytable">';

    foreach ($data as $value) {

      $price = round($value['price']);

      $content .='<tr>';
      $content .=' <td align="center">';
      $content .='<label class="container-check">';
      $content .=' <input type="checkbox"  name="checked_id[]" value="'.$value['product_id'].'">';
      $content .='<span class="checkmark"></span>';
      $content .='</label></td>';
      $content .='<td>'.$value['code'].'</td>';
      $content .='<td>'.$value['name'].'</td>';
      $content .='<td>'.$value['cat_name'].'</td>';
      $content .=' <td>'.$price.'</td>';
      $content .='<td>'.$value['discount'].' %</td>';
      $content .='<td><a title="Chi tiết" ';
      $content .='class="btn btn-sm btn-outline-info" ';
      $content  .='href="'.base_url().'Sanpham/detailitem/'.$value['product_id'].'">';
      $content .='<i class="fas fa-pen"></i></a> ';
      $content .='| <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"';
      $content .=' data-toggle="modal" data-target="#del'.$value['product_id'].'">';
      $content .=' <i class="fas fa-trash-alt"></i>';
      $content .=' </button></td>';
      $content .='</tr>';
    }

    $content .='</tbody>';

    return $content;
  }

  public function getby_catcom($cat,$com)
  {

   if(is_array($cat)||is_array($com)){

    $this->db->where_in('catalog_id', $cat);
    $this->db->where_in('com_id', $com);

  }

  $this->db->order_by(self::TABLE_NAME.'.created', 'desc');
  $data = $this->db->get(self::TABLE_NAME)->result_array();

  $content='';

  foreach ($data as $value) {

    if ($value['discount'] != 0) {

     $content.='<div class="col_1_of_3 span_1_of_3">';
     $content.='<a href="'.base_url().'Trangsanpham/getdetail/'.$value['code'].'">';
     $content.='<div class="inner_content clearfix">';
     $content.='<div class="product_image">';
     $content.='<img src="'.base_url().'assets/uploads/'.$value['img_link'].'" alt="@@@@" ';
     $content.='style="width: 250px; height: 250px;"/>';
     $content.='</div>';
     $content.='<div class="sale-box1"><span class="on_sale title_shop">';
     $content.='Sale</span></div>';
     $content.='<div class="price">';
     $content.='<div class="cart-left">';
     $content.='<p class="title">'.$value['name'].'</p>';
     $content.='<div class="price1" style="font-size: 15px;">';
     $content.='<span class="reducedfrom">';
     $content.=''.(round($value['price'])).'đ</span>';
     $content.='<span class="actual">';
     $content.=''.($this->Cal_price($value['price'] , $value['discount'])).'đ';
     $content.='</span>';
     $content.='</div>';
     $content.='</div>';
     $content.='<div class="cart-right"></div>';
     $content.='<div class="clear"></div>';
     $content.='</div>';
     $content.='</div></a></div>';

   }

   else {

     $content.='<div class="col_1_of_3 span_1_of_3">';
     $content.='<a href="'.base_url().'Trangsanpham/getdetail/'.$value['code'].'">';
     $content.='<div class="inner_content clearfix">';
     $content.='<div class="product_image">';
     $content.='<img  src="'.base_url().'assets/uploads/'.$value['img_link'].'" ';
     $content.=' alt="@@@@" style="width: 250px; height: 250px;"/>';
     $content.='</div>';
     $content.='<div class="price">';
     $content.='<div class="cart-left">';
     $content.='<p class="title">'.$value['name'].'</p>';
     $content.='<div class="price1" style="font-size: 15px;">';
     $content.='<span class="actual">';
     $content.=''.(round($value['price'])).'đ';
     $content.='</span>';
     $content.='</div>';
     $content.='</div>';
     $content.='<div class="cart-right"> </div>';
     $content.='<div class="clear"></div>';
     $content.='</div>';
     $content.='</div></a></div>';

   }

 }

 return $content;
}

private function Cal_price( $price , $discount)
{
  $res = round($price - ( ($discount * $price)/100 ));

  return $res;
}


}
?>
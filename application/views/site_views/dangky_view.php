<?php include 'header.php' ?>

<?php include 'header_top.php' ?>
<?php include 'header_bottom.php' ?>

<!-- st: main -->
<form action="<?= base_url() ?>Khachhang/register" method="post">
	<div class="login">
		<div class="wrap">
			<div class="col_1_of_login span_1_of_login">
				<h4 class="title">Chào mừng</h4>
				<p>Đã có tài khoản !!! 
					<a href="<?= base_url() ?>Khachhang/load_logincustomer" 
						class="mybutton" style="width: auto; margin-left: 150px;">Đăng nhập</a></p>
						<div class="clear"></div>
					</div>
					<div class="col_1_of_login span_1_of_login">
						<div class="login-title">
							<h4 class="title">Đăng ký tài khoản</h4>
							<div id="loginbox" class="loginbox">
								<form action="" method="post" name="login" id="login-form">
									<fieldset class="input">
										<p id="login-form-username">
											<label for="email">Email</label>
											<input id="email" type="email" name="email" 
											class="inputbox" size="18" autocomplete="off">
										</p>
										<p id="login-form-password">
											<label for="pass">Mật khẩu</label>
											<input id="pass" type="password" name="password" 
											class="inputbox" size="18" autocomplete="off">
										</p>
										<p id="login-form-password">
											<label for="name">Họ tên</label>
											<input id="name" type="text" name="name" class="inputbox" size="18" autocomplete="off">
										</p>
										<p id="login-form-password">
											<label for="phone">Số điện thoại</label>
											<input id="phone" type="text" name="phone" 
											class="inputbox" size="18" autocomplete="off">
										</p>
										<p id="login-form-password">
											<label for="address">Địa chỉ</label>
											<input id="address" type="text" name="address"
											class="inputbox" size="18" autocomplete="off">
										</p>
										<p id="login-form-password">
											<label for="code">Mã xác nhận</label>
											<input id="code" type="text" name="code"
											class="inputbox" size="18" autocomplete="off" readonly>
										</p>
										<div class="remember">
											<button type="button" class="mybutton" id="send" 
											style="width: auto; font-style: normal;" onclick="btn_clk();">
										Gửi mã</button>
										<input type="submit" name="Submit" class="button" value="ĐĂNG KÝ">
										<div class="clear">
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</form>

	<script type="text/javascript" charset="utf-8">

		function btn_clk(argument) {

			var path = '<?= base_url() ?>';

			var email = $('#email').val().trim();
			var name = $('#name').val().trim();
			var phone = $('#phone').val().trim();
			var pass = $('#pass').val().trim();
			var address = $('#address').val().trim();

			if (email != '' && phone != '' && pass != '' && name != '' && address != '') {

				alert("Đợi một chút !!!");

				$.ajax({
					url: path+'Khachhang/send',
					type: 'post',
					data: {email: email},
				})
				.done(function() {
					console.log("success");
				})
				.fail(function() {
					console.log("error");
				})
				.always(function(res) {
					console.log("complete");
					alert(res);
					$('#send').css('visibility', 'hidden');
					$('#code').attr('readonly', false);
				});
			}

			else { 
               
               alert("Vui lòng điền đầy đủ các trường còn thiếu !!!");
			
			}
		}

	</script>

	<!-- end: main -->
	<?php include 'footer.php' ?>
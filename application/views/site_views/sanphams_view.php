<?php include 'header.php' ?>

<?php include 'header_top.php' ?>

<?php include 'header_bottom.php' ?>

<?php include 'slide.php' ?>

<!-- st: main -->

<div class="main">
	<div class="wrap">

		<div class="cont span_2_of_3">

			<h2 class="head">Toàn bộ sản phẩm</h2>

			<div class="mens-toolbar">
				<div class="pager">
					<ul class="dc_pagination dc_paginationA dc_paginationA06" 
					style="margin-left: 100px;">
					<?= $page ?>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div id="content">
			<?php foreach ($all as $value): ?>

				<?php if ($value['discount'] > 0) { ?>

				<div class="col_1_of_3 span_1_of_3"> 
					<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $value['code'] ?>">
						<div class="inner_content clearfix">
							<div class="product_image">
								<img src="<?= base_url() ?>assets/uploads/<?= $value['img_link'] ?>" alt="@@@@" style="width: 250px; height: 250px;"/>
							</div>
							<div class="sale-box1"><span class="on_sale title_shop">
							Sale</span></div>	
							<div class="price">
								<div class="cart-left">
									<p class="title"><?= $value['name'] ?></p>
									<div class="price1" style="font-size: 15px;">
										<span class="reducedfrom">
											<?php echo(round($value['price'])) ?>đ</span>
											<span class="actual">
												<?php 
												echo(Cal_price($value['price'] , $value['discount']));
												?>đ
											</span>
										</div>
									</div>
									<div class="cart-right"> </div>
									<div class="clear"></div>
								</div>				
							</div>
						</a>
					</div>

					<?php } else { ?>

					<div class="col_1_of_3 span_1_of_3"> 
						<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $value['code'] ?>">
							<div class="inner_content clearfix">
								<div class="product_image">
									<img 
									src="<?= base_url() ?>assets/uploads/<?= $value['img_link'] ?>"
									alt="@@@@" style="width: 250px; height: 250px;"/>
								</div>
								<div class="price">
									<div class="cart-left">
										<p class="title"><?= $value['name'] ?></p>
										<div class="price1" style="font-size: 15px;">
											<span class="actual">
											<?php echo($this->cart->format_number(round($value['price']))) ?>đ
											</span>
										</div>
									</div>
									<div class="cart-right"> </div>
									<div class="clear"></div>
								</div>				
							</div>
						</a>
					</div>

					<?php } ?>

				<?php endforeach ?>

			</div>

			<div class="clear"></div>



		</div>

		<?php include 'right_sidebar.php' ?>

		<div class="clear"></div>
	</div>
</div>
</div>


<?php 

function Cal_price( $price , $discount)
{
	$res = round($price - ( ($discount * $price)/100 ));

	echo $res;
}

?>



<!-- end: main -->


<?php include 'footer.php' ?>
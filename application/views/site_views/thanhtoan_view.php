<?php include 'header.php' ?>

<?php include 'header_top.php' ?>
<?php include 'header_bottom.php' ?>

<!-- st:main -->

<div class="register_account">
	<div class="wrap">

		<?php if(count($this->cart->contents()) != 0) { ?>

		<?php if(!empty($_SESSION['customer'])) { ?>

		<?php foreach ($cus as $value): ?>

			<h4 class="title">Thanh toán đơn hàng</h4>
			<form action="<?= base_url() ?>Thanhtoan/submit" method="post" id="form">
				<div class="col_1_of_2 span_1_of_2">
					<div><input type="text" placeholder="Họ tên" name="hoten" id="hoten"
						value="<?= $value['name'] ?>"></div>

						<div><input type="text" placeholder="Địa chỉ giao hàng" name="diachi" id="diachi"
							value="<?= $value['address'] ?>"></div>

							<div><input type="email" placeholder="E-Mail" name="email" id="email"
								value="<?= $value['email'] ?>"></div>

								<div><input type="text" placeholder="Số điện thoại" name="sodienthoai" id="sodienthoai"
									value="<?= $value['phone'] ?>"></div>
								</div>

								<div class="col_1_of_2 span_1_of_2">	
									<div><input type="text" placeholder="Mã giảm giá" name="magiamgia" id="magiamgia"></div>
									<div>
										<select id="hinhthuc" name="hinhthuc" class="frm-field required" style="width: 530px;">
											<option value="Thanh toán trực tiếp" selected>Thanh toán trực tiếp</option>
											<option value="Thanh toán khi nhận hàng">Thanh toán khi nhận hàng</option>
										</select></div>		        
										<div style="display: inline-block;">
											<input type="text" placeholder="Mã xác thực" style="width: 380px;" readonly 
											name="maxacthuc" id="maxacthuc">
											<button class="mybutton" id="btn_send"  type="button" 
											style="height: auto; width: auto;vertical-align: center;"
											onclick="sendcode_Clk();">
											Gửi mã
										</button>
									</div>
									<div>
									</div>
									<textarea name="yeucaukhac" placeholder="Yêu cầu khác" rows="5"></textarea>
								</div>
								<button class="grey" onclick="btn_submitClk();" type="button">Xác nhận</button>
								<div class="clear"></div>
							</form>

						<?php endforeach ?>


						<?php } else { ?>

						<h4 class="title">Thanh toán đơn hàng</h4>
						<form action="<?= base_url() ?>Thanhtoan/submit" method="post" id="form">
							<div class="col_1_of_2 span_1_of_2">
								<div><input type="text" placeholder="Họ tên" name="hoten" id="hoten"></div>

								<div><input type="text" placeholder="Địa chỉ giao hàng" name="diachi" id="diachi"></div>

								<div><input type="email" placeholder="E-Mail" name="email" id="email"></div>

								<div><input type="text" placeholder="Số điện thoại" name="sodienthoai" id="sodienthoai"></div>
							</div>

							<div class="col_1_of_2 span_1_of_2">	
								<div><input type="text" placeholder="Mã giảm giá" name="magiamgia"
									id="magiamgia" readonly title="Bạn phải đăng nhập để nhập mã"></div>
									<div>
										<select id="hinhthuc" name="hinhthuc" class="frm-field required" style="width: 530px;">
											<option value="Thanh toán trực tiếp" selected>Thanh toán trực tiếp</option>
											<option value="Thanh toán khi nhận hàng">Thanh toán khi nhận hàng</option>
											<option value="Thanh toán trực tuyến">Thanh toán trực tuyến</option>
										</select></div>		        
										<div style="display: inline-block;">
											<input type="text" placeholder="Mã xác thực" style="width: 380px;" readonly 
											name="maxacthuc" id="maxacthuc">
											<button class="mybutton" id="btn_send"  type="button" 
											style="height: auto; width: auto;vertical-align: center;"
											onclick="sendcode_Clk();">
											Gửi mã
										</button>
									</div>
									<div>
									</div>
									<textarea name="yeucaukhac" placeholder="Yêu cầu khác" rows="5"></textarea>
								</div>
								<button class="grey" onclick="btn_submitClk();" type="button">Xác nhận</button>
								<div class="clear"></div>
							</form>

							<?php }; ?>

							<?php } else { ?>

							<h4 class="title">Không có sản phẩm nào trong giỏ</h4>
							<a href="<?= base_url() ?>Trangsanpham" style="color: red;">Tiếp tục mua hàng</a>

							<?php } ?>

						</div>
					</div>


					<script  type="text/javascript" charset="utf-8" >

						function sendcode_Clk(argument) {

							var path = '<?= base_url() ?>';

							var email = $('#email').val().trim();
							var name = $('#hoten').val();
							var address = $('#diachi').val();
							var discount = $('#magiamgia').val().trim();


							if (email != '' && name != '' && address != '') {

								$.ajax({
									url: path+'Thanhtoan/send_code',
									type: 'post',
									data: {email: email , name: name , discount: discount , address: address},
								})
								.done(function() {
									console.log("success");
								})
								.fail(function() {
									console.log("error");
								})
								.always(function(res) {
									console.log("complete");

									alert(res);
									$('#maxacthuc').attr('readonly', false);
									$('#btn_send').attr('disabled', true);
								});

								alert("Vui lòng đợi !!!");
							}

							else {

								alert("Vui lòng điền vào các trường bỏ trống !!!");
							}

						}

					</script>

					<script type="text/javascript" charset="utf-8">

						function btn_submitClk(argument) {

							var s_code = $('#maxacthuc').val().trim();

							if(s_code != '') {

								$('#form').submit();
							}

							else {

								alert("Vui lòng điền mã xác nhận !!!");
							}
						}

					</script>


					<!-- end:main -->

					<?php include 'footer.php' ?>
<div class="header-bottom">
	<div class="wrap">
		<div class="header-bottom-left">
			<div class="logo" style="width: 190px;">
				<a href="<?= base_url() ?>Trangchu">
					<img src="<?= base_url() ?>/assets/site/images/logoAMD.png" 
					alt="img"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">
						<li class="active grid"><a href="<?= base_url() ?>Trangchu">Trang chủ</a></li>
						<li><a class="color7" href="<?= base_url() ?>Tranggioithieu">Giới thiệu</a></li>
						<li><a class="color6" href="<?= base_url() ?>Trangsanpham">Sản phẩm</a></li>
						<li><a class="color7" href="<?= base_url() ?>Trangtintuc">Tin tức</a></li>
					</ul>
				</div>
			</div>
			<div class="header-bottom-right">
				<div class="search">	  
					<input type="text" name="s" class="textbox" value="Tìm kiếm" id="searchbox" 
					onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Tìm kiếm';}">
					<input type="submit" value="Subscribe" id="submit" name="submit">
					<div id="response"> </div>
				</div>
				<div class="tag-list">
					<ul class="icon1 sub-icon1 profile_img">
						<li><a class="active-icon c1" href="<?= base_url() ?>Thanhtoan"> </a>
						</li>
					</ul>
					<ul class="icon1 sub-icon1 profile_img">
						<li><a class="active-icon c2" href="<?= base_url() ?>Giohang"> </a>
						</li>
					</ul>
					<ul class="last"><li><a href="#">Cart(<?= count($this->cart->contents()) ?>)</a></li></ul>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
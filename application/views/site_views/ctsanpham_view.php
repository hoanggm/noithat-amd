<?php include 'header.php'; ?>

<?php include 'header_top.php' ?>

<?php include 'header_bottom.php' ?>


<!-- st:main -->

<div class="mens">     
	<div class="main">
		<div class="wrap">

			<?php foreach ($detail as $value): ?>


				<div class="cont span_2_of_3">
					<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a>
									<img class="etalage_thumb_image" 
									src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
									class="img-responsive" />
									<img class="etalage_source_image" 
									src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>" 
									class="img-responsive"
									title="" />
								</a>
							</li>
							<li>
								<img class="etalage_thumb_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive" />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive" 
								title="" />
							</li>
							<li>
								<img class="etalage_thumb_image"
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
							</li>
							<li>
								<img class="etalage_thumb_image"
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>" 
								class="img-responsive"  />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="desc1 span_3_of_2">
						<h3 class="m_3" style="color: red; font-size: 30px;">
							Sản phẩm : <?= $value['name'] ?>
						</h3>
						
						<p class="m_5">Giá : 
							<?= $this->cart->format_number(Cal_price($value['price'],$value['discount'])) ?> 
						đ</p>

						<div class="btn_form">
							<form method="post" 
							action="<?= base_url() ?>Giohang/addto_cart/<?= $value['code'] ?>">
							<input type="submit" value="thêm vào giỏ" >
						</form>
					</div>
				</div>
				<div class="clear"></div>	

				<div class="clients">
					<h3 class="m_3">* Sản phẩm cùng loại</h3>
					<ul id="flexiselDemo3">

						<?php foreach ($same as $sa): ?>
							<li><img src="<?= base_url() ?>/assets/uploads/<?= $sa['img_link'] ?>" 
								style="width: 100px; height: 100px;"/>
								<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $sa['code'] ?>">
									<?= $sa['name'] ?></a><p>
										<?= $this->cart->format_number(Cal_price($sa['price'],$sa['discount'])) ?> đ
									</p></li>
								<?php endforeach ?>

							</ul>
							<script type="text/javascript">
								$(window).load(function() {
									$("#flexiselDemo1").flexisel();
									$("#flexiselDemo2").flexisel({
										enableResponsiveBreakpoints: true,
										responsiveBreakpoints: { 
											portrait: { 
												changePoint:480,
												visibleItems: 1
											}, 
											landscape: { 
												changePoint:640,
												visibleItems: 2
											},
											tablet: { 
												changePoint:768,
												visibleItems: 3
											}
										}
									});

									$("#flexiselDemo3").flexisel({
										visibleItems: 5,
										animationSpeed: 1000,
										autoPlay: false,
										autoPlaySpeed: 3000,    		
										pauseOnHover: true,
										enableResponsiveBreakpoints: true,
										responsiveBreakpoints: { 
											portrait: { 
												changePoint:480,
												visibleItems: 1
											}, 
											landscape: { 
												changePoint:640,
												visibleItems: 2
											},
											tablet: { 
												changePoint:768,
												visibleItems: 3
											}
										}
									});

								});
							</script>
							<script type="text/javascript"
							src="<?= base_url() ?>/assets/site/js/jquery.flexisel.js">
						</script>
					</div>

					<div class="toogle">
						<h3 class="m_3">* Chi tiết sản phẩm</h3>
						<p class="m_text">
							<p style="color: red;">* Thiết kế : <?= $value['com_name'] ?></p>
							<br>
							<p style="color: red;">* Lượt xem : <?= $value['count_view'] ?></p>
							<br>
							<?= $value['detail'] ?>
						</p>
					</div>
				</div>

			<?php endforeach ?>

			<div class="rsidebar span_1_of_left">
				<div class="top-border"> </div>
				<div class="border">
					<link href="<?= base_url() ?>assets/site/css/default.css" 
					rel="stylesheet" type="text/css" media="all" />
					<link href="<?= base_url() ?>assets/site/css/nivo-slider.css" 
					rel="stylesheet" type="text/css" media="all" />
					<script src="<?= base_url() ?>assets/site/js/jquery.nivo.slider.js"></script>
					<script type="text/javascript">
						$(window).load(function() {
							$('#slider').nivoSlider();
						});
					</script>
					<div class="slider-wrapper theme-default">
						<div id="slider" class="nivoSlider">
							<img src="<?= base_url() ?>/assets/uploads/noithat1.jpg" />
							<img src="<?= base_url() ?>/assets/uploads/imagesq.jpg" />
							<img src="<?= base_url() ?>/assets/uploads/noithat3.jpg" />
						</div>
					</div>
					
				</div>
				<div class="top-border"> </div>
				<div class="sidebar-bottom">
					<h2 class="m_1">Tin mới<br></h2>
					<p class="m_text">Đăng ký để nhận thông báo</p>
					<img src="<?= base_url() ?>assets/uploads/noithat3.jpg" alt="@@@@">
					<div style="height: 20px;"></div>
					<div class="subscribe">
						<form>
							<a class="mybutton" style="width: auto; background: #20c997;"
							href="<?= base_url() ?>Khachhang/load_register">Đăng ký</a>
						</form>
					</div>
				</div>
			</div>


			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php 

function Cal_price( $price , $discount)
{
	$res = round($price - ( ($discount * $price)/100 ));

	return $res;

}

?>


<!-- end:main -->

<?php include 'footer.php' ?>
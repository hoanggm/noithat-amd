<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>


     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <button type="button" class="btn btn-success btn-round" 
        data-toggle="modal" data-target="#add_modal">
        <i class="fas fa-plus"></i>  &#160; Thêm
      </button>
      &#160;
      <button type="button" class="btn btn-danger btn-round"
      data-toggle="modal" data-target="#multidel">
      <i class="fas fa-trash-alt"></i>  &#160; Xóa
    </button>
  </div>
</div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('hang_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('hang_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('hang_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('hang_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('hang_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('hang_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Hang/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng thiết kế  </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Tên thiết kế </th>
                <th>Mô tả</th>
                <th style="width: auto;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>

                  <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  
                  <td><?= $value['name'] ?></td>
                  <td><?= $value['description'] ?></td>
                  <td>
                    <button type="button" title="Chi tiết" class="btn btn-sm btn-outline-info" 
                    data-toggle="modal" data-target="#edit<?= $value['id'] ?>">
                    <i class="fas fa-pen"></i>
                  </button> 
                  | <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"
                  data-toggle="modal" data-target="#del<?= $value['id'] ?>">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>
            </tr>

            <?php $i++; ?>
          <?php endforeach ?>

        </tbody></table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">

       <span id="count" class="badge badge-danger" style="font-size: 15px;">
         <?= $i; ?> bản ghi / trang </span>
         <ul class="pagination pagination-sm m-0 float-right">

           <?php echo $page; ?>

         </ul>
       </div>
     </div>

     <!-- end: table -->

   </form>

   <!-- /.card -->
 </div>
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>


<!--st: add/edit modal -->
<div class="modal fade" id="add_modal" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url() ?>Hang/add" method="post" id="insert_form">

          <div class="form-group">
            <label for="tenHang">Tên thiết kế</label>
            <input type="text" class="form-control" id="tenHang" 
            name="tenHang" placeholder="Tên thiết kế">
          </div>

          <div class="form-group">
            <label for="website">Mô tả</label>
            <input type="text" class="form-control" id="website"  
            name="description" placeholder="Mô tả">
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Submit_insert()">Lưu</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>

      </form>

    </div>
  </div>
</div>


<script type="text/javascript" charset="utf-8" >

  function Submit_insert(argument) {

   var va_name = $('#tenHang').val().trim();
   var va_web = $('#website').val().trim();

   if(va_name != '' && va_web != '')
   {
    $('#add_modal').modal('hide');
    $('#insert_form').submit();
  }

  else
  {

    if(va_name == ''){ $("#tenHang").addClass('is-invalid');}
    else { $("#tenHang").removeClass('is-invalid');}

    if(va_web == ''){ $("#website").addClass('is-invalid');}
    else { $("#website").removeClass('is-invalid');}
  }

}

</script>

<!-- end: add_modal -->

<!-- st: edit_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="edit<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Hang/update" method="post" 
            id="update_form<?= $value['id'] ?>">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">

            <div class="form-group">
              <label for="tenHang<?= $value['id'] ?>">Tên thiết kế</label>
              <input type="text" class="form-control" id="tenHang<?= $value['id'] ?>" 
              name="tenHang" placeholder="Tên thiết kế"   
              value="<?= $value['name'] ?>">
            </div>

            <div class="form-group">
              <label for="website<?= $value['id'] ?>">Mô tả</label>
              <input type="text" class="form-control" id="website<?= $value['id'] ?>"  
              name="description" placeholder="Mô tả"   
              value="<?= $value['description'] ?>">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="Submit_update_<?= $value['id'] ?>()">
            Lưu</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

  <script type="text/javascript" charset="utf-8" >

    function Submit_update_<?= $value['id'] ?>(argument) {

     var va_name = $('#tenHang<?= $value['id'] ?>').val().trim();
     var va_web = $('#website<?= $value['id'] ?>').val().trim();

     if(va_name != '' && va_web != '')
     {
      $('#edit<?= $value['id'] ?>').modal('hide');
      $('#update_form<?= $value['id'] ?>').submit();
    }

    else
    {

      if(va_name == ''){ $("#tenHang<?= $value['id'] ?>").addClass('is-invalid');}
      else { $("#tenHang<?= $value['id'] ?>").removeClass('is-invalid');}

      if(va_web == ''){ $("#website<?= $value['id'] ?>").addClass('is-invalid');}
      else { $("#website<?= $value['id'] ?>").removeClass('is-invalid');}
    }

  }

</script>



<?php endforeach ?>
<!-- end: edit_modal -->

<!-- st: delete_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="del<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Hang/delete" method="post">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">
            <p>Xóa bản ghi này ????</p>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Xóa</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>

<!-- end: delete_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->


<?php include 'footer.php'; ?>
</div>
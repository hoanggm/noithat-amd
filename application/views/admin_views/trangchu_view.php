<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;" onload="loadBody()">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>
    
    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <?php include 'header_content.php'; ?>

     <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-8 card">
            <div class="card-header no-border">
              <div class="d-flex justify-content-between">
                <h3 class="card-title">Thống kê đơn hàng - sản phẩm</h3>
              </div>
            </div>
            <div class="card-body">
              <div class="d-flex">
                <p class="d-flex flex-column">
                </p>
                <p class="ml-auto d-flex flex-column text-right">
                  <span class="text-success">
                  </span>
                  <span class="text-muted"></span>
                </p>
              </div>
              <!-- /.d-flex -->

              <div class="position-relative mb-4">
                <canvas id="visitors-chart" height="200"></canvas>
              </div>

              <div class="d-flex flex-row justify-content-end">
                <span class="mr-2">
                  <i class="fas fa-square text-gray"></i> Số đơn hàng
                </span>
                <span >
                  <i class="fas fa-square text-primary"></i> Số sản phẩm
                </span>
              </div>
            </div>
          </div>
          <!-- /.card -->
          <div class="col-md-3">

           <div class="row">
             <div class="d-card small-box bg-info">
              <div class="inner">
                <h3><?= $countOrders ?></h3>
                <p>Đơn hàng mới</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?= base_url() ?>/Giaodich" 
                class="small-box-footer">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="row">
              <div class="d-card small-box bg-warning">
                <div class="inner">
                  <h3><?= $countUsers ?></h3>
                  <p>Khách hàng mới</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?= base_url() ?>/Khachhang" class="small-box-footer">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

          </div> 



        </div>


      </div><!-- /.container-fluid -->
    </section>

    <script type="text/javascript" charset="utf-8" async defer>
      function loadBody() {
        var path='<?= base_url() ?>';
        var listMonth=[];
        var listSoluonghang=[]; 
        var listSoluongdon=[]; 
        $.ajax({
          url: path+'Thongke/getThongke', 
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          type: 'post',
          success: function (res) {
            var listBase=res[0].data;
            if(listBase){
             listMonth=listBase.thang;
             listSoluonghang=listBase.soluongban;
             listSoluongdon=listBase.soluongdon;
           }


           var areaChartData = {
            labels  : listMonth,
            datasets: [
            {
              label               : 'Số sản phẩm',
              backgroundColor     : '#007bff',
              borderColor         : '#007bff',
              pointRadius          : true,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : listSoluonghang
            },
            {
              label               : 'Số đơn hàng',
              backgroundColor     : 'black',
              borderColor         : 'black',
              pointRadius         : true,
              pointColor          : '#3b8bba',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : listSoluongdon
            },
            ]
          }

          var areaChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                gridLines : {
                  display : false,
                }
              }],
              yAxes: [{
                gridLines : {
                  display : false,
                }
              }]
            }
          }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $('#visitors-chart').get(0).getContext('2d')
    var lineChartOptions = areaChartOptions
    var lineChartData = areaChartData
    lineChartData.datasets[0].fill = false;
    lineChartData.datasets[1].fill = false;
    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, { 
      type: 'line',
      data: lineChartData, 
      options: lineChartOptions
    })
  }
});

      }
    </script>

    <script type="text/javascript" defer async>
      $(function () {

    //     var areaChartData = {
    //       labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //       datasets: [
    //       {
    //         label               : 'Digital Goods',
    //         backgroundColor     : '#007bff',
    //         borderColor         : '#007bff',
    //         pointRadius          : true,
    //         pointColor          : '#3b8bba',
    //         pointStrokeColor    : 'rgba(60,141,188,1)',
    //         pointHighlightFill  : '#fff',
    //         pointHighlightStroke: 'rgba(60,141,188,1)',
    //         data                : [28, 48, 40, 19, 86, 27, 90]
    //       },
    //       {
    //         label               : 'Electronics',
    //         backgroundColor     : 'black',
    //         borderColor         : 'black',
    //         pointRadius         : true,
    //         pointColor          : '#3b8bba',
    //         pointStrokeColor    : '#c1c7d1',
    //         pointHighlightFill  : '#fff',
    //         pointHighlightStroke: 'rgba(220,220,220,1)',
    //         data                : [65, 59, 80, 81, 56, 55, 40]
    //       },
    //       ]
    //     }

    //     var areaChartOptions = {
    //       maintainAspectRatio : false,
    //       responsive : true,
    //       legend: {
    //         display: false
    //       },
    //       scales: {
    //         xAxes: [{
    //           gridLines : {
    //             display : false,
    //           }
    //         }],
    //         yAxes: [{
    //           gridLines : {
    //             display : false,
    //           }
    //         }]
    //       }
    //     }

    // //-------------
    // //- LINE CHART -
    // //--------------
    // var lineChartCanvas = $('#visitors-chart').get(0).getContext('2d')
    // var lineChartOptions = areaChartOptions
    // var lineChartData = areaChartData
    // lineChartData.datasets[0].fill = false;
    // lineChartData.datasets[1].fill = false;
    // lineChartOptions.datasetFill = false

    // var lineChart = new Chart(lineChartCanvas, { 
    //   type: 'line',
    //   data: lineChartData, 
    //   options: lineChartOptions
    // })
  })
</script>

</div>


<?php include 'footer.php'; ?>


<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>


     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <a class="btn btn-success btn-round" href="<?= base_url() ?>Sanpham/open_insertform">
          <i class="fas fa-plus"></i>  &#160; Thêm
        </a>
        &#160;
        <button type="button" class="btn btn-danger btn-round"
        data-toggle="modal" data-target="#multidel">
        <i class="fas fa-trash-alt"></i>  &#160; Xóa
      </button>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('sp_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('sp_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('sp_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('sp_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('sp_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('sp_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Sanpham/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng sản phẩm  </h3>

            <div class="card-tools">
              <div class="row">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <select id="filter_cat" class="form-control-sm" style="width: 100px;">
                    <option value="">Danh mục</option>
                    <?php showCategories($cats) ?>
                  </select>

                  <div class="input-group-append">
                    <button id="btn_filter_cat" onclick="btn_filtercat_Clicked();" 
                    type="button" class="btn btn-default"><i class="fas fa-filter"></i></button>
                  </div>
                </div>

                <div class="input-group input-group-sm" style="width: 150px;">
                  <select id="filter_com" class="form-control-sm" style="width: 100px;">
                    <option value="">Thiết kế</option>

                    <?php foreach ($coms as $com): ?>

                      <option value="<?= $com['id'] ?>"><?= $com['name'] ?></option>

                    <?php endforeach ?>

                  </select>

                  <div class="input-group-append">
                    <button id="btn_filter_com" onclick="btn_filtercom_Clicked();" 
                    type="button" class="btn btn-default"><i class="fas fa-filter"></i></button>
                  </div>

                </div>
              </div>

            </div>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Mã sản phẩm</th>
                <th>Tên sản phẩm</th>
                <th>Danh mục</th>
                <th>Giá</th>
                <th>Giảm giá</th>
                <th style="width: auto;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>
                  <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['product_id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>

                  <td><?= $value['code'] ?></td>
                  <td><?= $value['name'] ?></td>
                  <td><?= $value['cat_name'] ?></td>
                  <td><?= round($value['price']) ?></td>
                  <td><?= $value['discount'] ?> %</td>

                  <td>
                    <a title="Chi tiết" class="btn btn-sm btn-outline-info" 
                    href="<?= base_url() ?>Sanpham/detailitem/<?= $value['product_id'] ?>">
                    <i class="fas fa-pen"></i>
                  </a> 
                  | <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"
                  data-toggle="modal" data-target="#del<?= $value['product_id'] ?>">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>

            </tr>

            <?php $i++; ?>
          <?php endforeach ?>

        </tbody></table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">

       <span id="count" class="badge badge-danger" style="font-size: 15px;">
         <?= $i; ?> bản ghi / trang </span>
         <ul class="pagination pagination-sm m-0 float-right">

           <?php echo $page; ?>

         </ul>
       </div>
     </div>

     <!-- end: table -->

   </form>

   <!-- /.card -->
 </div>
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>

<!-- st: delete_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="del<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Sanpham/delete" method="post">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">
            <p>Xóa bản ghi này ????</p>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Xóa</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>

<!-- end: delete_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->

<!-- scpt: filter -->
<script type="text/javascript" charset="utf-8">

  function btn_filtercat_Clicked() {

    var id = $('#filter_cat').val();
    var path = "<?= base_url() ?>";

    if(id != '') {

      $.ajax({
        url: path+'Sanpham/filter_danhmuc',
        type: 'post',
        dataType: 'html',
        data: {id: id},
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(res) {

        $('#mytable').remove();
        $('#table').html(res);

      });

    }

    else {

      window.location.reload();
    }

  };

</script>

<script type="text/javascript" charset="utf-8">

  function btn_filtercom_Clicked() {

    var id = $('#filter_com').val();
    var path = "<?= base_url() ?>";

    if(id != '') {

      $.ajax({
        url: path+'Sanpham/filter_hang',
        type: 'post',
        dataType: 'html',
        data: {id: id},
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(res) {


        $('#mytable').remove();
        $('#table').html(res);

      });

    }

    else {

      window.location.reload();
    }

  };

</script>

<!-- end scrp -->


<?php 

function showCategories($cats, $parent_id = 0, $char = '',$selected_id = 0)
{
  foreach ($cats as $item)
  {
          // Nếu là chuyên mục con thì hiển thị
    if ($item['parent_id'] == $parent_id)
    {
      if ($item['parent_id']==$selected_id && $selected_id != 0) {
        echo '<option selected value="'.$item['id'].'">';
        echo $char . $item['name'];
        echo '</option>';

      }
      else {
        echo '<option value="'.$item['id'].'">';
        echo $char . $item['name'];
        echo '</option>';
      }

              // Xóa chuyên mục đã lặp
      unset($cats['id']);

              // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
      showCategories($cats, $item['id'], $char.'---| ');
    }
  }
}

?>

<?php include 'footer.php'; ?>

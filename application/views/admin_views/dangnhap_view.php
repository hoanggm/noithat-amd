
<?php include 'header.php'; ?>

<style type="text/css" >

.card{
  animation-name: animationtest;
  animation-duration: 2s;
}

@keyframes animationtest {
  0%{
    opacity: 0;
    transform: translate(0, -100px);
  }

  80%{
    transform: translate(0, -20px);
  }

  100%{
    opacity: 1;
    transform: translate(0,0)
  }


}
</style>

<body class="hold-transition login-page">
  <div class="login-box">


    <div class="card">
      <div class="card-header" style="background: #319DDC; color: white;">
       <i class="fa fa-user" aria-hidden="true"></i> |&#160;&#160;&#160;<b>Webbanhang</b>
     </div>
     <div class="card-body login-card-body rounded border-primary">

       <p class="login-box-msg">Đăng nhập để bắt đầu</p>

       <form action="<?= base_url() ?>Admin/login" method="post" id="login_form">
        <div class="input-group mb-3">
          <?php if(empty($_SESSION['username'])) { ?>
          <input type="text" class="form-control" placeholder="Tên đăng nhập" 
          id="username">
          <?php } else { ?>
           <input type="text" class="form-control" placeholder="Tên đăng nhập" 
          id="username" value="<?= $_SESSION['username'] ?>">
          <?php } ?>
          <div class="input-group-append input-group-text">
            <span class="fas fa-user"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Mật khẩu" 
          id="password">
          <div class="input-group-append input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>

        <div class="social-auth-links text-center mb-3">
          <p></p>
          <button class="btn btn-block btn-primary"
          id="login_but" type="button" onclick="LoginClick()">
           Đăng nhập
         </button>
       </div>

     </form>
     <!-- /.social-auth-links -->

     <p class="mb-1">
      <a href="#">Quên mật khẩu ?</a>
    </p>
  </div>
  <!-- /.login-card-body -->
</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?= base_url() ?>assets/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/admin/plugins/jquery-base64/jquery.base64.js"></script>
<script src="<?= base_url() ?>assets/admin/plugins/jquery-base64/jquery.base64.min.js"></script>

<script type="text/javascript" charset="utf-8" >

  function LoginClick(argument) {

    var ranu = Math.floor(Math.random() * 10); 
    var ranp = Math.floor(Math.random() * 10); 

    var username = $('#username').val();
    var password = $('#password').val();

    for (var i = 0; i <= ranu; i++) {

      username = $.base64.encode(username);
    }

    for (var i = 0; i <= ranp; i++) {

      password = $.base64.encode(password);
    }

    var input = $("<input>").attr("type", "hidden").attr("name", "username").val(username);
    $('#login_form').append(input);

    var input = $("<input>").attr("type", "hidden").attr("name", "password").val(password);
    $('#login_form').append(input);

    var input = $("<input>").attr("type", "hidden").attr("name", "ran1").val(ranu);
    $('#login_form').append(input);

    var input = $("<input>").attr("type", "hidden").attr("name", "ran2").val(ranp);
    $('#login_form').append(input);

    $('#login_form').submit();
  }

</script>

</body>
</html>

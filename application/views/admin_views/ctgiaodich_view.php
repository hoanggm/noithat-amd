<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

      <form action="<?= base_url() ?>Giaodich/update_statuss" method="post" id="myform">

        <input type="hidden" name="id" value="<?= $tran_id ?>">

        <div class="row">
          <div class="col-md-5">
           <?php include 'header_content.php'; ?>
         </div>

         <div class="col-md-7" style="margin-top: 20px;">
          <div class="row" >

            <?php if($status == 0) { ?>

            <select name="capnhattrangthai" style="width: 200px; margin-right: 5px;" class="form-control">
              <?php if($pay == 'Thanh toán khi nhận hàng') { ?>
              <option value="1">Đã chuyển hàng</option>
              <option value="2">Đã thanh toán</option>
              <?php } elseif($pay == 'Thanh toán trực tiếp') { ?>
              <option value="2">Đã thanh toán</option>
              <?php } else { ?>
              <option value="2">Đã thanh toán</option>
              <?php } ?>
            </select>
            <button class="btn btn-info" id="btn_submit" style="width: 200px;"
            onclick="submit_form();" type="button">
            <i class="fa fa-arrow-left" aria-hidden="true"></i>
            &#160;
            Cập nhật trạng thái
          </button>

          <?php } elseif($status == 1) { ?>

          <select name="capnhattrangthai" style="width: 200px; margin-right: 5px;" class="form-control">
            <option value="2">Đã thanh toán</option>
          </select>
          <button class="btn btn-info" id="btn_submit" style="width: 200px;"
          onclick="submit_form();" type="button">
          <i class="fa fa-arrow-left" aria-hidden="true"></i>
          &#160; 
          Cập nhật trạng thái
        </button>

        <?php } else { ?>
        <div style="width: 400px;"></div>
        <?php } ?>

        &#160;
        <a class="btn btn-secondary" href="<?= base_url() ?>Giaodich" style="width: 150px;">
          Trở về
        </a>

      </div>

    </div>
  </div>

</form>

<section class="content">
  <div class="container-fluid">

    <div class="row" style="height: 10px;"></div>

    <?php foreach ($tran as $info): ?>

      <div class="row">
        <div class="card text-white bg-light mb-3">
          <div class="card-header">
            <h3 class="card-title">Thông tin khách hàng</h3>
          </div>
          <div class="card-body">
            <div class="col-12">

              <div class="row">
                <div class="col-md-12">
                  <p style="font-size: 16px;">Họ tên khách hàng : &#160;&#160;
                    <?= $info['user_name'] ?></p>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <p style="font-size: 16px;">Số điện thoại : &#160;&#160;
                      <?= $info['user_phone'] ?></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <p style="font-size: 16px;">E-mail : &#160;&#160;
                        <?= $info['user_email'] ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <p style="font-size: 16px;">Địa chỉ giao hàng : &#160;&#160;
                          <?= $info['user_address'] ?></p>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <p style="font-size: 16px;">Hình thức thanh toán : &#160;&#160;
                            <?= $info['payment'] ?></p>
                          </div>
                        </div>

                        <?php if($info['discount_code'] != '') { ?>

                        <div class="row">
                          <div class="col-md-12">
                            <p style="font-size: 16px; color: red;">Tổng tiền trước giảm giá : &#160;&#160;
                              <?= round($sum) ?> đ</p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12">
                              <p style="font-size: 16px;">Mã giảm giá : &#160;&#160;
                                <?= $info['discount_code'] ?>&#160;( - <?= $info['percent'] ?>%)</p>
                              </div>
                            </div>

                            <?php } else { ?>

                            <div class="row">
                              <div class="col-md-12">
                                <p style="font-size: 16px; color: red;">Tổng tiền trước giảm giá : 
                                  &#160;&#160;
                                  <?= round($sum) ?> đ</p>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <p style="font-size: 16px;">Mã giảm giá : &#160;&#160;
                                  Không có mã / Mã hết hạn</p>
                                </div>
                              </div>

                              <?php } ?>

                              <div class="row">
                                <div class="col-md-12">
                                  <p style="font-size: 16px; color: red;">Tổng tiền sau giảm giá : &#160;&#160;
                                    <?= round($info['amount']) ?> đ</p>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>

                      <?php endforeach ?>


                      <div class="row">
                        <div class="col-12">

                         <!-- st:table -->

                         <div class="card">
                          <div class="card-header">
                            <h3 class="card-title">Các sản phẩm trong đơn hàng</h3>
                          </div>
                          <!-- /.card-header -->
                          <div class="card-body">
                            <table id="table" class="table table-bordered" style="border-radius: 6px;">
                              <thead>
                                <th>Mã sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                                <th>Thành tiền</th>
                              </thead>
                              <tbody id="mytable">

                                <?php $i = 0; ?>

                                <?php foreach ($detail as $value): ?>

                                  <tr>
                                    <td><?= $value['products_code'] ?></td>
                                    <td><?= $value['pr_name'] ?></td>
                                    <td><?= round($value['price']) ?> đ</td>
                                    <td><?= $value['qty'] ?></td>
                                    <td><?= round($value['amount']) ?> đ</td>
                                  </tr>

                                  <?php $i++; ?>
                                <?php endforeach ?>

                              </tbody></table>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer clearfix">
                            </div>
                          </div>

                          <!-- end: table -->

                          <!-- /.card -->
                        </div>
                      </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                  </section>

                </div>

                <script  type="text/javascript" charset="utf-8">
                  function submit_form(argument) {

                   $('#myform').submit();

                 }
               </script>



               <?php include 'footer.php'; ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url() ?>assets/admin/dist/img/user2.png"
          class="img-circle elevation-2" alt="User_Image">
        </div>
        <div class="info">
          <a href="#" class="d-block" style="color: white;">
            <?php if (!is_null($_SESSION['username'])): ?>
                <?php echo 'Quản trị hệ thống' ?>
            <?php endif ?>
          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <li class="nav-item" style="margin-top: 10px;">
          <a href="<?= base_url() ?>Admin" class="nav-link">
            <i class="far nav-icon fas fa-home"></i>
            <p>Trang chủ</p>
          </a>
        </li>
        <li class="nav-item" style="margin-top: 20px;">
         <a href="<?= base_url() ?>Danhmuc" class="nav-link">
          <i class="far nav-icon fas fa-list-alt"></i>
          <p>Danh mục</p>
        </a>
      </li>
      <li class="nav-item" style="margin-top: 20px;">
       <a href="<?= base_url() ?>Hang" class="nav-link">
        <i class="far nav-icon fas fa-torii-gate"></i>
        <p>Thiết kế</p>
      </a>
    </li>
    <li class="nav-item" style="margin-top: 20px;">
      <a href="<?= base_url() ?>Sanpham" class="nav-link">
        <i class="far nav-icon fas fa-cube"></i>
        <p>Sản phẩm</p>
      </a>
    </li>

    <li class="nav-item" style="margin-top: 20px;">
      <a href="<?= base_url() ?>Tintuc" class="nav-link">
        <i class="nav-icon fas fa-pen-square"></i>
        <p>Tin tức</p>
      </a>
    </li>
   
     <li class="nav-item has-treeview" style="margin-top: 20px;">
      <a href="#" class="nav-link">
        <i class="far nav-icon fas fa-file-invoice-dollar"></i>
        <p>Giao dịch</p>
      </a>

      <ul class="nav nav-treeview" style="display: hidden; font-size: 14px;">
        <li class="nav-item">
          <a href="<?= base_url() ?>Giaodich" class="nav-link">
            &#160; &#160; &#160; &#160; &#160;
            <p>Giao dịch</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?= base_url() ?>Giamgia" class="nav-link">
            &#160; &#160; &#160; &#160; &#160;
            <p>Mã giảm giá</p>
          </a>
        </li>

      </ul>
    </li>
    <li class="nav-item has-treeview" style="margin-top: 20px;">
      <a href="#" class="nav-link">
        <i class="far nav-icon fas fa-address-book"></i>
        <p>Tài khoản</p>
      </a>

      <ul class="nav nav-treeview" style="display: hidden; font-size: 14px;">
        <li class="nav-item">
          <a href="<?= base_url() ?>Taikhoan" class="nav-link">
            &#160; &#160; &#160; &#160; &#160;
            <p>Quản trị viên</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="<?= base_url() ?>Khachhang" class="nav-link">
            &#160; &#160; &#160; &#160; &#160;
            <p>Khách hàng</p>
          </a>
        </li>

      </ul>
    </li>

  </ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>

<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>


     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <button type="button" class="btn btn-success btn-round" 
        data-toggle="modal" data-target="#add_modal">
        <i class="fas fa-plus"></i>  &#160; Thêm
      </button>
      &#160;
      <button type="button" class="btn btn-danger btn-round"
      data-toggle="modal" data-target="#multidel">
      <i class="fas fa-trash-alt"></i>  &#160; Xóa
    </button>
  </div>
</div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('dm_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('dm_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('dm_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('dm_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('dm_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('dm_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->


  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Danhmuc/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng danh mục  </h3>
            <div class="card-tools">
              <div class="input-group input-group-sm">
                <select id="filter" class="form-control-sm">
                  <option value="">-- Lọc --</option>

                  <?php foreach ($all_parent as $filter_item): ?>

                    <option value="<?= $filter_item['id'] ?>">
                      <?= $filter_item['name'] ?>
                    </option>

                  <?php endforeach ?>
                </select>

                <div class="input-group-append">
                  <button id="btn_filter" onclick="btn_filterClicked();" 
                  type="button" class="btn btn-default"><i class="fas fa-filter"></i></button>
                </div>
              </div>

            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Tên danh mục</th>
                <th>Danh mục cha</th>
                <th style="width: auto;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>

                   <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>



                  <td><?= $value['name'] ?></td>

                  <?php kiemtra_danhmuccha($value['pa_name']); ?>


                  <td>
                    <button type="button" title="Chi tiết" class="btn btn-sm btn-outline-info" 
                    data-toggle="modal" data-target="#edit<?= $value['id'] ?>">
                    <i class="fas fa-pen"></i>
                  </button> 
                  | <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"
                  data-toggle="modal" data-target="#del<?= $value['id'] ?>">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>
            </tr>

            <?php $i++; ?>
          <?php endforeach ?>

        </tbody></table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">

       <span id="count" class="badge badge-danger" style="font-size: 15px;">
         <?= $i; ?> bản ghi / trang </span>
         <ul class="pagination pagination-sm m-0 float-right">

           <?php echo $page; ?>

         </ul>
       </div>
     </div>

     <!-- end: table -->

   </form>

   <!-- /.card -->
 </div>
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>


<!--st: add modal -->
<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url() ?>Danhmuc/add" method="post" id="insert_form">

          <div class="form-group">
            <label for="tendanhmuc">Tên danh mục</label>
            <input type="text" class="form-control" id="tendanhmuc" 
            name="tendanhmuc" placeholder="Tên danh mục">
          </div>

          <div class="form-group">
            <label for="ghichu">Ghi chú</label>
            <input type="text" class="form-control" id="ghichu"  
            name="ghichu" placeholder="Ghi chú" >
          </div>

          <div class="form-group">
            <label for="danhmuccha">Danh mục cha</label>
            <select name="danhmuccha" class="form-control">
              <option value="0">-- Trống --</option>
              <?php showCategories($alldata);  ?>
            </select>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Submit_insert()">Lưu</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>

      </form>

    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8" >

  function Submit_insert(argument) {

   var va_name = $('#tendanhmuc').val().trim();
   var va_note = $('#ghichu').val().trim();

   if(va_name != '' && va_note != '')
   {
    $('#add_modal').modal('hide');
    $('#insert_form').submit();
  }

  else
  {

    if(va_name == ''){ $("#tendanhmuc").addClass('is-invalid');}
    else { $("#tendanhmuc").removeClass('is-invalid');}

    if(va_note == ''){ $("#ghichu").addClass('is-invalid');}
    else { $("#ghichu").removeClass('is-invalid');}
  }

}

</script>

<!-- end: add_modal -->

<!-- st: edit_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="edit<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Danhmuc/update" method="post" 
            id="update_form<?= $value['id'] ?>">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">

            <div class="form-group">
              <label for="tendanhmuc<?= $value['id'] ?>">Tên danh mục</label>
              <input type="text" class="form-control" id="tendanhmuc<?= $value['id'] ?>" 
              name="tendanhmuc" placeholder="Tên danh mục" 
              value="<?= $value['name'] ?>">
            </div>

            <div class="form-group">
              <label for="ghichu<?= $value['id'] ?>">Ghi chú</label>
              <input type="text" class="form-control" id="ghichu<?= $value['id'] ?>"  
              name="ghichu" placeholder="Ghi chú" 
              value="<?= $value['note'] ?>">
            </div>

            <div class="form-group">
              <label for="danhmuccha<?= $value['id'] ?>">Danh mục cha</label>
              <select name="danhmuccha" class="form-control">

                <?php if(!is_null($value['pa_name'])) { ?>
                <option value="<?= $value['pa_id'] ?>"><?= $value['pa_name'] ?></option>
                <?php } else { ?>
                <option value="<?= $value['pa_id'] ?>">-- Trống --</option>
                <?php } ?>

                <option value="0">-- Trống --</option>
                <?php showCategories($alldata);  ?>
              </select>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="Submit_update_<?= $value['id'] ?>()">
            Lưu</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>


  <script type="text/javascript" charset="utf-8" >

    function Submit_update_<?= $value['id'] ?>(argument) {

     var va_name = $('#tendanhmuc<?= $value['id'] ?>').val().trim();
     var va_note = $('#ghichu<?= $value['id'] ?>').val().trim();

     if(va_name != '' && va_note != '')
     {
      $('#edit<?= $value['id'] ?>').modal('hide');
      $('#update_form<?= $value['id'] ?>').submit();
    }

    else
    {

      if(va_name == ''){ $("#tendanhmuc<?= $value['id'] ?>").addClass('is-invalid');}
      else { $("#tendanhmuc<?= $value['id'] ?>").removeClass('is-invalid');}

      if(va_note == ''){ $("#ghichu<?= $value['id'] ?>").addClass('is-invalid');}
      else { $("#ghichu<?= $value['id'] ?>").removeClass('is-invalid');}
    }

  }

</script>



<?php endforeach ?>
<!-- end: edit_modal -->

<!-- st: delete_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="del<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Danhmuc/delete" method="post">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">
            <p>Xóa bản ghi này ????</p>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Xóa</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>

<!-- end: delete_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->

<?php 

function showCategories($alldata, $parent_id = 0, $char = '',$selected_id = 0)
{
  foreach ($alldata as $item)
  {
          // Nếu là chuyên mục con thì hiển thị
    if ($item['parent_id'] == $parent_id)
    {
      if ($item['parent_id']==$selected_id && $selected_id != 0) {
       echo '<option selected value="'.$item['id'].'">';
       echo $char . $item['name'];
       echo '</option>';

     }
     else {
       echo '<option value="'.$item['id'].'">';
       echo $char . $item['name'];
       echo '</option>';
     }

              // Xóa chuyên mục đã lặp
     unset($alldata['id']);

              // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
     showCategories($alldata, $item['id'], $char.'---| ');
   }
 }
}

function kiemtra_danhmuccha($danhmuccha)
{
  if (is_null($danhmuccha)) {
    echo '<td style="color: red;">'.'Không có danh mục cha'.'</td>';
  }
  else {

    echo ' <td>'.$danhmuccha.'</td>';

  }
}

?>



<!-- scpt: filter -->
<script type="text/javascript" charset="utf-8">

  function btn_filterClicked() {

    var pa_id = $('#filter').val();
    var path = "<?= base_url() ?>";

    if(pa_id != '') {

      $.ajax({
        url: path+'Danhmuc/filter',
        type: 'post',
        dataType: 'html',
        data: {pa_id: pa_id},
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(res) {

        console.log("complete");

        $('#mytable').remove();
        $('#table').html(res);

      });

    }

    else {

      window.location.reload();
    }

  };

</script>
<!-- end scrp -->


<?php include 'footer.php'; ?>

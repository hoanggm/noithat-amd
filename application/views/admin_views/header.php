<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>NoithatAMD.com-Admin</title>
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo(base_url()) ?>assets/admin/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo(base_url()) ?>assets/admin/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		NoithatAMD.com-Admin
	</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
	name='viewport' />
	<!-- Font Awesome -->
	<link href="<?= base_url() ?>assets/admin/plugins/fontawesome-free/css/all.css" 
	rel="stylesheet" 
	media="all">
	<link href="<?= base_url() ?>assets/admin/plugins/fontawesome-free/css/fontawesome.css" 
	rel="stylesheet" media="all">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/sweetalert2/sweetalert2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/toastr/toastr.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<script src="<?= base_url() ?>assets/admin/plugins/summernote/summernote-bs4.min.js"></script>
	<link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/summernote/summernote-bs4.css">
	<script src="<?php echo(base_url()) ?>ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/plugins/chart.js/Chart.min.css">

	<!-- jQuery -->
	<script src="<?= base_url() ?>assets/admin/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url() ?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- FastClick -->
	<script src="<?= base_url() ?>assets/admin/plugins/fastclick/fastclick.js"></script>
	<!-- SweetAlert2 -->
	<script src="<?= base_url() ?>assets/admin/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?= base_url() ?>assets/admin/plugins/toastr/toastr.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>assets/admin/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url() ?>assets/admin/dist/js/demo.js"></script>

	<!-- chart js -->
	<script src="<?= base_url() ?>assets/admin/plugins/chart.js/Chart.min.js"
		type="text/javascript"></script>


		<style type="text/css" media="all">

		.d-card{
			margin-left: 20px;
			width: 100% !important;
		}

		.btn-round {

			width: 150px;
			border-radius: 18px;
		}

		/* Customize the label (the container) */
		.container-check {
			display: block;
			position: relative;
			padding-left: 35px;
			margin-bottom: 12px;
			cursor: pointer;
			font-size: 22px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		/* Hide the browser's default checkbox */
		.container-check input {
			position: absolute;
			opacity: 0;
			cursor: pointer;
			height: 0;
			width: 0;
		}

		/* Create a custom checkbox */
		.checkmark {
			position: absolute;
			top: 0;
			left: 0;
			border-color: gray;
			border-style: solid;
			border-radius: 5px;
			height: 20px;
			width: 20px;
			background-color: #eee;
		}

		/* On mouse-over, add a grey background color */
		.container-check:hover input ~ .checkmark {
			background-color: #ccc;
		}

		/* When the checkbox is checked, add a blue background */
		.container-check input:checked ~ .checkmark {
			background-color: #2196F3;
		}

		/* Create the checkmark/indicator (hidden when not checked) */
		.checkmark:after {
			content: "";
			position: absolute;
			display: none;
		}

		/* Show the checkmark when checked */
		.container-check input:checked ~ .checkmark:after {
			display: block;
		}

		/* Style the checkmark/indicator */
		.container-check .checkmark:after {
			left: 5px;
			top: 2px;
			align-content: center;
			width: 5px;
			height: 10px;
			border: solid white;
			border-width: 0 2px 2px 0;
			-webkit-transform: rotate(45deg);
			-ms-transform: rotate(45deg);
			transform: rotate(45deg);
		}


		/* The switch - the box around the slider */
		.switch {
			position: relative;
			display: inline-block;
			width: 50px;
			height: 23px;
		}

		/* Hide default HTML checkbox */
		.switch input {
			opacity: 0;
			width: 0;
			height: 0;
		}

		/* The slider */
		.slider {
			position: absolute;
			cursor: pointer;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #ccc;
			-webkit-transition: .4s;
			transition: .4s;
		}

		.slider:before {
			position: absolute;
			content: "";
			height: 16px;
			width: 15px;
			left: 4px;
			bottom: 4px;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
		}

		input:checked + .slider {
			background-color: #20c997;
		}

		input:focus + .slider {
			box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
			-webkit-transform: translateX(26px);
			-ms-transform: translateX(26px);
			transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
			border-radius: 34px;
		}

		.slider.round:before {
			border-radius: 50%;
		}

	</style>

</head>